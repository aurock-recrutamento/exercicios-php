<?php

/**
 * Exibir a lista abaixo como uma tabela html
 * Exibir a linha em verde usando <tr class="verde"> quando a coluna "diferença" for positiva (> 0)
 * Exibir a linha em vermelho usando <tr class="vermelho"> quando a coluna "diferença" for positiva (< 0)
 * Colunas: Posição, Time, Jogos, Rounds Ganhos - Rounds Perdidos, Diferença
 */


$challengers = [
    // posicao, time, vitorias, derrotas, rounds ganhos, rounds perdidos
    ['1', 'Team Liquid', '3', '0', '51', '30'],
    ['1', 'Ninjas in Pyjamas', '3', '0', '60', '43'],
    ['3', 'Astralis', '3', '1', '74', '48', '+26'],
    ['3', 'compLexity Gaming', '3', '1', '55', '54'],
    ['3', 'HellRaisers', '3', '1', '69', '70'],
    ['6', 'BIG', '2', '2', '60', '49'],
    ['6', 'North', '2', '2','71', '64'],
    ['6', 'TyLoo', '2', '2', '73', '71'],
    ['6', 'Team Spirit', '2', '2', '49', '49'],
    ['6', 'Vega Squadron', '2', '2', '67', '68'],
    ['6', 'OpTic Gaming', '2', '2', '71', '80'],
    ['12', 'Gambit Esports', '1', '3', '55', '61'],
    ['12', 'Rogue', '1', '3', '56', '67'],
    ['12', 'Renegades', '1', '3', '45', '63'],
    ['15', 'Space Soldiers', '0', '3', '36', '51'],
    ['15', 'Virtus.pro', '0', '3', '24', '48'],
];

?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />

        <title>Challengers - Major</title>

        <style>
            table tr.verde {
                background: #2ECC40;
            }

            table tr.vermelho {
                background: #FF4136;
                color: #FFF;
            }
        </style>
    </head>
    <body>
        <table>
            <thead>
                <tr>
                    <th>Posição</th>
                    <th>Time</th>
                    <th>Jogos</th>
                    <th>Rounds Ganhos - Rounds Perdidos</th>
                    <th>Diferença</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            </tbody>
        </table>
        <a href="11-index.php"><br>Voltar</a>
    </body>
</html>