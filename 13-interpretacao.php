<?php

/**
 * Interpretar o código abaixo e escrever o objetivo do código em comentarios, preferencialmente no inicio ou final do arquivo.
 * 
 * O objetivo do código é fazer a criação de um inicio de projeto, criando os diretorios necessários, e os arquivos necessarios para começar a trabalhar.
 * Ao criar o ambiente ocorre a instalação automatica do Composer, ferramente para gerenciamento de bibliotecas, mantendo-as sempre atualizadas.
 */


function showError($num, $message)
{
    echo "\n----------------------------- A error was occurred --------------------------\n";
    echo "- ". $message;
    echo "\n-----------------------------------------------------------------------------\n";
}

set_error_handler('showError');

if (!isset($argv[1])) {
        showError(0, "A namespace must be specified.");
        exit;
}

$namespace = strtolower($argv[1]); // transforma argv[1] em letras minusculas
$namespace = ucfirst($namespace); // transforma a primeira letra das string em maiscula

$path = isset($argv[2]) ? $argv[2] : shell_exec('pwd'); // Se isset argv[2] = true, $path recebe argv[2] senão executa o terminal digita pwd e retorna um caminho.
$path = realpath($path); // $path recebe o caminho absoluto de path.

define("DS"            , DIRECTORY_SEPARATOR); //Define a constante DS com DIRECTORY_SEPARATOR
define("ROOT_FOLDER"   , $path .DS. strtolower($namespace)); //Define ROOT_FOLDER como $path(caminho absoluto) mais / $namespace em letras minusculas.
define("BIN_FOLDER"    , ROOT_FOLDER .DS. 'bin'); // Define BIN_FOLDER como ROOT_FOLDER mais / bin
define("PUBLIC_FOLDER" , ROOT_FOLDER .DS. 'public'); //Define PUBLIC_FOLDER mais / public

define("SRC_FOLDER"    , ROOT_FOLDER .DS. 'src'); //Define SRC_FOLDER como ROOT_FOLDER mais / mais src
define("PROJECT_FOLDER", SRC_FOLDER .DS. $namespace); //Define PROJECT_FOLDER como SRC_FOLDER mais / $namespace

define("TESTS_FOLDER"  , ROOT_FOLDER .DS. 'tests'); //Define TESTS_FOLDER como ROOT_FOLDER mais / mais tests
define("TESTS_SRC_FOLDER"  , TESTS_FOLDER .DS. 'src'); //Define TESTS_SRC_FOLDER como TEST_FOLDER mais / mais src
define("PROJECT_TESTS_FOLDER", TESTS_SRC_FOLDER .DS. $namespace); //Define PROJECT_TESTS_FOLDER como TESTS_SRC_FOLDER mais / mais $namespace



echo "Stating...\n";
mkdir(ROOT_FOLDER); // cria um diretorio com a constante ROOT_FOLDER

$composer = fopen(ROOT_FOLDER .DS. 'composer.json', 'w'); // $composer "abre" o arquivo que esta no diretorio ROOT_FOLDER chamado composer.json, com permissão para leitura.
fputs($composer, // abre o arquivo que esta em composer e digita o comando abaixo.
<<<FILE    
{
        "autoload" : {
                "psr-0" : { "$namespace" :  "src/" }
        },
        "require" : {

        }
}
FILE
); //uso do heredoc
fclose($composer); //fecha o arquivo de $composer.



echo "Bin Folder...\n";
mkdir(BIN_FOLDER); //Cria o diretorio da constante BIN_FOLDER

echo "Downloading bin/composer.phar...\n";
$ch = curl_init('http://getcomposer.org/composer.phar'); //$ch recebe uma URL para download do composer.
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // curl_setopt seta uma opção no caso a returntransfer 

$composer = curl_exec($ch); // executa a curl para dwonload do arquivo.
curl_close($ch); // encerra a curl

file_put_contents(BIN_FOLDER .DS. 'composer.phar', $composer); // Cria um arquivo com o nome composer.phar e adciona o conteudo de $composer.



echo "Public folder...\n";
mkdir(PUBLIC_FOLDER); // Cria um diretorio com o conteudo da constante PUBLIC_FOLDER
$index = fopen(PUBLIC_FOLDER .DS. 'index.php', 'w'); //Abre o diretorio PUBLIC_FOLDER cria um arquivo chamado index.php com permissão para escrita.
fputs($index, //abre o arquivo index.php e adiciona o conteudo abaixo.
<<<FILE
<?php

echo "Hello $namespace!";
FILE
); // encerra o heredoc.
fclose($index); // fecha o arquivo;

$index = fopen(PUBLIC_FOLDER .DS. '.htaccess', 'w'); //Abre o diretorio PUBLIC_FOLDER  e cria o arquivo htacces com premissão para escrita.
fclose($index); // fecha o arquivo.



echo "Source Folder...\n";
mkdir(SRC_FOLDER); // cria o diretorio com o conteudo da constante SRC_FOLDER
mkdir(PROJECT_FOLDER); // Cria o diretorio com o conteudo da constante PROJECT_FOLDER

$app = fopen(PROJECT_FOLDER .DS. 'Application.php', 'w'); //Abre o diretorio PROJECT_FOLDER e cria o arquivo Application.php com permissão para escrita.
fputs($app,  //abre o arquivo Application.php e adiciona o conteudo do heredoc.
<<<FILE
<?php

namespace $namespace;

class Application
{
        public function __construct()
        {

        }
}
FILE
);
fclose($app); //fecha o arquivo.



echo "Tests folder...\n"; 
mkdir(TESTS_FOLDER); //Cria o diretorio com o conteudo de TESTS_FOLDER
mkdir(TESTS_SRC_FOLDER); //Cria o diretorio com o conteudo de TESTS_SRC_FOLDER
mkdir(PROJECT_TESTS_FOLDER); //Cria o diretorio com o conteudo de PROJECT_TESTS_FOLDER

$app = fopen(PROJECT_TESTS_FOLDER .DS. 'ApplicationTest.php', 'w'); //Abre o diretorio de PROJECT_TESTS_FOLDER e cria o arquivo ApplicationTest.php com permissão para escrita.
fputs($app, //Abre o arquivo ApplicationTest.php e adiciona o conteudo do heredoc.
<<<FILE
<?php

namespace $namespace;

class ApplicationTest extends \PHPUnit_Framework_TestCase
{
        public function setup()
        {

        }

        public function teardown()
        {

        }
}
FILE
);
fclose($app); //fecha o arquivo.



echo "Project {$namespace} is complete.\n"; //mensagem de sucesso ao criar o role.