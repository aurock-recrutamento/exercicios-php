<?php

function connect() {
    return new PDO('sqlite:dbs/contatos.sq3');
}

function select($conn, $sql) {
    $stm = $conn->query($sql);

    $data = [];
    while ($row = $stm->fetch(PDO::FETCH_ASSOC)) {
        $data[] = $row;
    }

    return $data;
}

function insert($conn, $sql) {
    return (bool) $conn->query($sql);
}

function createTable ($conn) {
    return $conn->exec(
        'CREATE TABLE IF NOT EXISTS CADASTRO(
            id integer PRIMARY KEY AUTOINCREMENT,
            nome varchar(50),
            sobrenome varchar(50),
            email varchar(50),
            telefone varchar(9),
            mensagem varchar (200)
        );'
    );
}